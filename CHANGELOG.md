# CHANGELOG

## v1.0.6 (2025-02-18)

### Fixed

- Updated base container image to `alpine:3.21.3`.

## v1.0.5 (2025-01-17)

### Fixed

- Updated base container image to `alpine:3.21.2`.

## v1.0.4 (2024-10-18)

### Fixed

- Fixed pipeline `needs` for `deploy_branch` job with `container_scanning`
  enabled.

## v1.0.3 (2024-10-18)

### Fixed

- Removed `container_scanning` disable in triggered pipeline. (#1)

## v1.0.2 (2024-09-24)

### Fixed

- Fixed container image OCI `annotations` to match project. Required pinning
  `Dockerfile` base image tag and digest.

## v1.0.1 (2024-07-22)

### Fixed

- Updated `trigger` pipelines to include `lint_prose` and `code_count` jobs to
  avoid false MR data. If these do not run on all pipelines, any data is
  considered a MR change.
- Moved release job overrides separate `include` to make trigger pipelines
  valid.

## v1.0.0 (2024-06-19)

Initial release
